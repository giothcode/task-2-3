// Task 3
/* Considerando los ejemplos:
Example 1:
Input: "nixi1"
Shift steps:
"ixi1n" - shift number one
"xi1ni" - shift number two
"i1nix" - shift number three
"1nixi" - shift number four
"nixi1" - shift number five shift number five, final
Output: 1 // Number of times shifted string is equal to original string
Example 2:
Input: "nixnix" Output: 2

Escribe una función que:
1. Tome un string como argumento
2. Cambiar el string letra por letra en cada loop
3. Retornar la cantidad de veces que el string desplazado es igual a la cadena original
*/

const changeCounter = (string) => {
  let original = string.slice();
  let run = true;
  let counter = 0;
  let letters = string.split("");
  while (run) {
    letters.unshift(letters.pop());
    let newString = letters.join("");
    if (newString === original) run = false;
    console.table({ original, newString });
    counter++;
  }
  return counter;
};
console.log("Respuesta", changeCounter("nixnix"));
