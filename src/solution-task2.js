module.exports.normalizeReceivedDataSolutionOne = (receivedData) => {
  const { users, messages } = receivedData;

  const findUser = (id) => {
    for (let i = 0; i < users.length; i++) {
      if (users[i].id === id) {
        return users[i];
      }
    }
  };

  for (let i = 0; i < messages.length; i++) {
    messages[i] = {
      messageText: messages[i].text,
      userFromName: findUser(messages[i].userId).name,
      usetToName: findUser(messages[i].receiverId).name,
    };
  }
  return messages;
};
module.exports.normalizeReceivedDataSolutionTwo = (receivedData) => {
  const { users, messages } = receivedData;

  const findUser = (id) => users.find((user) => user.id === id);

  return messages.map((message) => {
    return {
      messageText: message.text,
      userFromName: findUser(message.userId).name,
      usetToName: findUser(message.receiverId).name,
    };
  });
};
module.exports.normalizeReceivedDataSolutionThree = (receivedData) => {
  const { users, messages } = receivedData;

  let indexed = [];
  users.forEach((user) => {
    indexed[user.id] = { id: user.id, name: user.name };
  });

  return messages.map((message) => {
    return {
      messageText: message.text,
      userFromName: indexed[message.userId].name,
      usetToName: indexed[message.receiverId].name,
    };
  });
};
module.exports.normalizeReceivedDataSolutionFour = (receivedData) => {
  const { users, messages } = receivedData;

  let indexed = {};
  users.forEach((user) => {
    indexed[user.id] = { id: user.id, name: user.name };
  });

  return messages.map((message) => {
    return {
      messageText: message.text,
      userFromName: indexed[message.userId].name,
      usetToName: indexed[message.receiverId].name,
    };
  });
};
