/**
 * Escribe una función que itere en la data y retorne un solo array con data en el
 * formato:
 * [{ messageText, userFromName, userToName }]
 */
let { data } = require("./storage/data-task-2");
const { normalizeReceivedDataSolutionThree } = require("./solution-task2");

console.time("rendimiento");
console.log(normalizeReceivedDataSolutionThree(data));
console.timeEnd("rendimiento");
