module.exports.data = {
  users: [
    {
      id: 1,
      name: "dayana",
    },
    {
      id: 2,
      name: "katrina",
    },
    {
      id: 3,
      name: "gabriel",
    },
  ],
  messages: [
    {
      id: 1,
      text: "foo",
      userId: 1,
      receiverId: 2,
    },
    {
      id: 2,
      text: "bar",
      userId: 1,
      receiverId: 3,
    },
    {
      id: 3,
      text: "foo",
      userId: 1,
      receiverId: 2,
    },
    {
      id: 4,
      text: "bar",
      userId: 2,
      receiverId: 1,
    },
    {
      id: 5,
      text: "foo",
      userId: 2,
      receiverId: 3,
    },
    {
      id: 6,
      text: "bar",
      userId: 2,
      receiverId: 1,
    },
    {
      id: 7,
      text: "foo",
      userId: 1,
      receiverId: 2,
    },
    {
      id: 8,
      text: "bar",
      userId: 3,
      receiverId: 2,
    },
    {
      id: 9,
      text: "foo",
      userId: 2,
      receiverId: 3,
    },
    {
      id: 10,
      text: "bar",
      userId: 3,
      receiverId: 2,
    },
    {
      id: 11,
      text: "foo",
      userId: 1,
      receiverId: 3,
    },
    {
      id: 12,
      text: "bar",
      userId: 3,
      receiverId: 2,
    },
    {
      id: 13,
      text: "foo",
      userId: 2,
      receiverId: 3,
    },
    {
      id: 14,
      text: "bar",
      userId: 3,
      receiverId: 1,
    },
    {
      id: 15,
      text: "foo",
      userId: 1,
      receiverId: 3,
    },
    {
      id: 16,
      text: "bar",
      userId: 2,
      receiverId: 3,
    },
    {
      id: 17,
      text: "foo",
      userId: 3,
      receiverId: 1,
    },
    {
      id: 18,
      text: "bar",
      userId: 1,
      receiverId: 3,
    },
    {
      id: 19,
      text: "foo",
      userId: 3,
      receiverId: 2,
    },
    {
      id: 20,
      text: "bar",
      userId: 3,
      receiverId: 1,
    },
  ],
};
