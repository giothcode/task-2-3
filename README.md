# Task 2

> Solución lista dividida en dos archivos, en `task2.js` se muestra la ejecución, la solución está en `solution-task-2`. Se propone 4 soluciones para probar el rendimiento de las mismas y, se llega a proponer (para aplicar al bonus), que la mejor forma de hacerlo es mediante indexación, no por filtrado o algún método de búsqueda, puesto que, el iterar sobre los datos resulta en procesos más largos que indexar el valor. Las propuestas 3 y 4 mostraron menor tiempo de trabajo, ambas por indexación.

# Task 3

> Con el input propuesto se logró el siguiente output

```js
┌───────────┬──────────┐
│  (index)  │  Values  │
├───────────┼──────────┤
│ original  │ 'nixnix' │
│ newString │ 'xnixni' │
└───────────┴──────────┘
┌───────────┬──────────┐
│  (index)  │  Values  │
├───────────┼──────────┤
│ original  │ 'nixnix' │
│ newString │ 'ixnixn' │
└───────────┴──────────┘
┌───────────┬──────────┐
│  (index)  │  Values  │
├───────────┼──────────┤
│ original  │ 'nixnix' │
│ newString │ 'nixnix' │
└───────────┴──────────┘
Respuesta 3
```
